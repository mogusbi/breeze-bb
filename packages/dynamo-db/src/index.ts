/**
 * Breeze BB Dynamo DB adaptor
 * @module breeze-bb/dynamo-db
 */
export * from './dynamo-db.module';
export * from './dynamo-db';
